# Moodle Tracker

This Moodle plugin collects interaction events that occur when users interact with learning content in Moodle. For example, clicking, scrolling, and mouse movement are recorded. Each event is also enriched with contextual information about the learning activities before being sent. Once the enrichment of the individual event is complete, it is immediately sent to the Interaction Record Store (IRS) REST API and held there for subsequent use.

# Authors and acknowledgment

- Daniel Biedermann (since 2020)

# License
This Project is Open Source Software. You can redistribute it and modify it under the terms of the GNU General Public License version 3, as published by the Free Software Foundation, or any later version.

# Project status
This project is currently used in multiple research projects such as DiFA, AFLEK, ALICE (https://edutec.science/international-projects/) and Edutex (edutex.de).
